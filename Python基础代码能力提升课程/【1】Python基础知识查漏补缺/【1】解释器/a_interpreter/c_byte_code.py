import dis
from a_hello_world import *

# 生成pyc文件   python -m py_compile b_syntax_check.py


def print_hello_world():
    print("Hello World!")
    a = 1
    b = a + 2
    return b


dis.dis(print_hello_world)
