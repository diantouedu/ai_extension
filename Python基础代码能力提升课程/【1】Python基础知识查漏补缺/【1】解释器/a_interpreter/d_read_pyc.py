import dis
import marshal
import sys


path_pyc = "./__pycache__/b_syntax_check.cpython-310.pyc"
# path_pyc = "./__pycache__/a_hello_world.cpython-310.pyc"


with open(path_pyc, 'rb') as file:
    data = file.read()

# 反序列化
code = marshal.loads(data[16:])  # 前 16 字节包含了文件的魔术数字和时间戳

# 使用 dis 模块反汇编字节码
dis.dis(code)




