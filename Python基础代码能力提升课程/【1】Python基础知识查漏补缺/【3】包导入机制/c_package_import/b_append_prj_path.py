import os
import sys

# 获取项目根目录
current_dir = os.path.dirname(__file__)  # 获取当前文件的目录
prj_path = os.path.abspath(os.path.join(current_dir, '..', '..'))  # 计算相对路径

sys.path.append(prj_path)  # 项目路径添加到sys.path中
print("prj_path: {}\n".format(prj_path))

for path_package in sys.path:
    print(path_package)
